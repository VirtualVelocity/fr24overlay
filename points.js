var xmlDoc;
var latlon;
var flightPath = [];
var names = [];

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

jQuery.get("https://bitbucket.org/VirtualVelocity/fr24overlay/raw/dec5910271c90f6c6a65fc8d91c82dd6be45c267/YMML.xml", function(data) {
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(data, "text/xml");
    
    console.log("Loading STARS...");
    names.push('----- STARs:');
    
    var tags = xmlDoc.getElementsByTagName("Star"); // GET ALL STARS (RED COLOUR)
    
    for(var i = 0; i < tags.length; i++)
    {
        var route = [];
        var name = tags[i].getAttribute("Name");
        var points = tags[i].getElementsByTagName("Star_Waypoint");
        
        for(var index = 0; index < points.length; index++)
        {
            var lat = parseFloat(points[index].getElementsByTagName("Latitude")[0].innerHTML);
            var lng = parseFloat(points[index].getElementsByTagName("Longitude")[0].innerHTML);
            var latlon = {lat, lng};
            
            // ADD MARKER TO MAP!!
            var marker = new google.maps.Marker({ // Create the marker
                position: latlon,
                icon: {
                    path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW,
                    scale: 3
                },
                draggable: false,
                opacity: 0.4,
                title: points[index].getElementsByTagName("Name")[0].innerHTML,
                map: map
            });
            
            route.push(latlon);
        }
        
        route.push({lat: -37.6690, lng: 144.8410}); // Add melb to end of the route.
        
        flightPath[name] = new google.maps.Polyline({
            path: route,
            geodesic: true,
            strokeColor: '#B22222',
            strokeOpacity: 1.0,
            strokeWeight: 4
        });
        
        flightPath[name].setMap(null);
        names.push(name);
    }
    
    console.log("All STARS loaded. Loading SIDs...");
    names.push('----- SIDs:');
    
    var tags = xmlDoc.getElementsByTagName("Sid"); // GET ALL STARS (RED COLOUR)
    
    for(var i = 0; i < tags.length; i++)
    {
        var route = [];
        var name = tags[i].getAttribute("Name");
        var points = tags[i].getElementsByTagName("Sid_Waypoint");
        route.push({lat: -37.6690, lng: 144.8410}); // Add melb to start of the route.
        
        for(var index = 0; index < points.length; index++)
        {
            var lat = parseFloat(points[index].getElementsByTagName("Latitude")[0].innerHTML);
            var lng = parseFloat(points[index].getElementsByTagName("Longitude")[0].innerHTML);
            var latlon = {lat, lng};
            
            // ADD MARKER TO MAP!!
            var marker = new google.maps.Marker({ // Create the marker
                position: latlon,
                icon: {
                    path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW,
                    scale: 3
                },
                draggable: false,
                opacity: 0.4,
                title: points[index].getElementsByTagName("Name")[0].innerHTML,
                map: map
            });
            
            route.push(latlon);
        }
        
        flightPath[name] = new google.maps.Polyline({
            path: route,
            geodesic: true,
            strokeColor: '#008000',
            strokeOpacity: 1.0,
            strokeWeight: 4
        });
        
        flightPath[name].setMap(null);
        names.push(name);
    }
    
    console.log("All SIDs loaded. Done!");
    
    var fullList = "<br><span style='color: white;'>";

    for(var i = 0; i < names.length; i++)
    {
        fullList += '- <input name="' + names[i] + '" type="checkbox" id="' + names[i] + '">: ' + names[i] + '<br>';
    }
    fullList += '</span>';

    document.getElementById("secondaryView").innerHTML = fullList;
    document.getElementById("secondaryView").setAttribute('style', 'overflow-y: auto; height: 500px;');
});

$(document).ready(function(){
    $(document).on('change', 'input[type="checkbox"]', function(e){
        updatePolyline(e.currentTarget.name);
    });
});

function updatePolyline(name)
{
    var checkbox = document.getElementById(name).checked;
    
    if(checkbox)
    {
        flightPath[name].setMap(map);
    }
    else
    {
        flightPath[name].setMap(null);
    }
}